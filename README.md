# Configuration docker pour une démo Alternatiba

Copié depuis [docker-atelier](https://framagit.org/altermediatic/docker-atelier), la configuration docker pour monter un CHATONS avec peu d'efforts.

## Pré-requis

Pour pouvoir installer un CHATONS, vous devez disposer :
- d'une machine/VM avec une nouvelle installation de debian
- du domaine et des sous-domaines (cf. services présent dans `docker-compose.yml`) pointant sur l'ip publique de cette machine

## Installation

Pour installer et configurer un CHATONS, suivez [install.md](install.md) en tant que root.


